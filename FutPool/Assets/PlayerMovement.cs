using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float velocidad = 5f;

    private Rigidbody rb;

    public float rayDistance;

    public GameObject apuntar;

    public bool tocandoBolaBlanca;

    public bool Catch;

    public int jugadorID;


    public GameObject ball;

    public float ballDistance;

    public float ballForce;

    public bool holdingball;

    Rigidbody ballRB;

    public bool throwingBall;


    private void Awake()
    {
        ballRB = ball.GetComponent<Rigidbody>();
        rb = GetComponent<Rigidbody>();
    }

    public void LateUpdate()
    {
        if (holdingball)
        {
            ball.transform.position = apuntar.transform.position + this.transform.forward * ballDistance;

        }

        //if (throwingBall)
        //{
        //    ballRB.AddForce(rb.transform.forward * ballForce/*, ForceMode.VelocityChange*/);
        //    throwingBall = false;
        //}
    }

    public void Update()
    {

        CatchBall();
        DetectarBola();
        DetectarMirando();
    }

    private void FixedUpdate()
    {
      
        

        // Verificar el jugador actual y obtener las entradas de acuerdo a su ID
        float movimientoHorizontal = 0f;
        float movimientoVertical = 0f;

        if (jugadorID == 1)
        {
            movimientoHorizontal = Input.GetAxis("Horizontal");
            movimientoVertical = Input.GetAxis("Vertical");
        }
        //else if (jugadorID == 2)
        //{
        //    movimientoHorizontal = Input.GetAxis("Horizontal2");
        //    movimientoVertical = Input.GetAxis("Vertical2");
        //}

        Vector3 movimiento = new Vector3(movimientoHorizontal, 0f, movimientoVertical) * velocidad;

        rb.velocity = movimiento;
    }


    public void DetectarMirando()
    {
        Debug.DrawRay(apuntar.transform.position, apuntar.transform.TransformDirection(new Vector3(1, 0, 0)) * rayDistance, Color.red);
        RaycastHit hit;


        if (Physics.Raycast(apuntar.transform.position, apuntar.transform.TransformDirection(new Vector3(1, 0, 0)), out hit, rayDistance))
        {
            //ACA ESCRIBO LOS COMPORTAMIENTOS QUE TENGA SEGUN CON CADA OBJETO
            Debug.Log(hit.transform.tag);

            //if (!ChatController.guardado && hit.transform.tag == "Player")
            //{
            //    Debug.Log("Perdiste");
            //    Perdiste = true;

            //}

            //if (!playerController.vistaAlfrente && hit.transform.tag == "Player")
            //{
            //    Debug.Log("Perdiste");
            //    Perdiste = true;
            //}

        }
        //else
        //{
        //    Debug.Log("tranca");
        //}

    }




    public void DetectarBola()
    {
        if (Input.GetKeyDown(KeyCode.Joystick1Button0) && tocandoBolaBlanca)
        {
            Debug.Log("Catch");
            //Comienza a dar vueltas
            if (Catch)
            {
                holdingball = false;
                Catch = false;
                throwingBall = true;



                //ballRB = Instantiate(ballRB, apuntar.transform.position, Quaternion.identity);

                //if (throwingBall)
                //{
                    ballRB.AddForce(rb.transform.forward * ballForce, ForceMode.Impulse);
                    //throwingBall = false;
                //}

            }
            else
            {

                holdingball = true;

                Catch = true;
            }
            


        }
    }

    public void CatchBall()
    {
        if (Catch)
        {
            rb.isKinematic = true;

            transform.Rotate(new Vector3(0f, 100f, 0f) * Time.deltaTime);

        }
        else
        {
            rb.isKinematic = false;
            transform.Rotate(new Vector3(0f, 0, 0f));
        }
    }


    public void ThrowBall()
    {

    }









    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "BolaBlanca")
        {
            tocandoBolaBlanca = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "BolaBlanca")
        {
            tocandoBolaBlanca = false;
        }
    }

}
